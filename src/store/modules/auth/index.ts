import { computed, reactive, ref } from 'vue';
import { useRoute } from 'vue-router';
import { defineStore } from 'pinia';
import { useLoading } from '@sa/hooks';
import { SetupStoreId } from '@/enum';
import { useRouterPush } from '@/hooks/common/router';
import {fetchGetUserInfo, fetchLogin, fetchLoginWithCaptcha, fetchLoginWithEmail, fetchLogout} from '@/service/api';
import { localStg } from '@/utils/storage';
import { $t } from '@/locales';
import { useRouteStore } from '../route';
import { clearAuthStorage, getToken, getUserInfo } from './shared';

export const useAuthStore = defineStore(SetupStoreId.Auth, () => {
  const route = useRoute();
  const routeStore = useRouteStore();
  const { toLogin, redirectFromLogin } = useRouterPush(false);
  const { loading: loginLoading, startLoading, endLoading } = useLoading();

  const token = ref(getToken());

  const userInfo: Api.Auth.UserInfo = reactive(getUserInfo());

  /** is super role in static route */
  const isStaticSuper = computed(() => {
    const { VITE_AUTH_ROUTE_MODE, VITE_STATIC_SUPER_ROLE } = import.meta.env;

    return VITE_AUTH_ROUTE_MODE === 'static' && userInfo.roles.includes(VITE_STATIC_SUPER_ROLE);
  });

  /** Is login */
  const isLogin = computed(() => Boolean(token.value));

  /** Reset auth store */
  async function resetStore() {
    const authStore = useAuthStore();

    clearAuthStorage();

    authStore.$reset();

    if (!route.meta.constant) {
      await toLogin();
    }

    await routeStore.resetStore();
  }

  /**
   * Login
   *
   * @param userName User name
   * @param password Password
   * @param [redirect=true] Whether to redirect after login. Default is `true`
   */
  async function login(userName: string, password: string, redirect = true) {
    startLoading();

    const { data: tokenInfo, error } = await fetchLogin(userName, password);

    // console.log('tokenInfo==>', tokenInfo);

    if (!error) {
      const pass = await loginByToken(tokenInfo);

      if (pass) {
        await routeStore.initAuthRoute();

        if (redirect) {
          await redirectFromLogin();
        }

        if (routeStore.isInitAuthRoute) {
          window.$notification?.success({
            title: $t('page.login.common.loginSuccess'),
            content: $t('page.login.common.welcomeBack', { userName: userInfo.userName }),
            duration: 4500
          });
        }
      }
    } else {
      resetStore();
    }

    endLoading();
  }

  /**
   * Login
   *
   * @param userName User name
   * @param password Password
   * @param captcha
   * @param captchaKey
   * @param [redirect=true] Whether to redirect after login. Default is `true`
   */
  // eslint-disable-next-line max-params
  async function loginWithCaptcha(
    userName: string,
    password: string,
    captcha: string,
    captchaKey: string,
    redirect = true
  ) {
    startLoading();

    const { data: tokenInfo, error } = await fetchLoginWithCaptcha(userName, password, captcha, captchaKey);

    // console.log("tokenInfo==>", tokenInfo);

    if (!error) {
      const pass = await loginByToken(tokenInfo);

      if (pass) {
        await routeStore.initAuthRoute();

        if (redirect) {
          await redirectFromLogin();
        }

        if (routeStore.isInitAuthRoute) {
          window.$notification?.success({
            title: $t('page.login.common.loginSuccess'),
            content: $t('page.login.common.welcomeBack', { userName: userInfo.userName }),
            duration: 4500
          });
        }
      }
    } else {
      await resetStore();
    }

    endLoading();
  }

  // logout
  async function confirmLogout() {
    await fetchLogout();
    await resetStore();

  }

  async function loginByToken(tokenInfo: Api.Auth.LoginToken) {
    // 1. stored in the localStorage, the later requests need it in headers
    localStg.set('token', tokenInfo.token);
    localStg.set('refreshToken', tokenInfo.refreshToken);

    const { data: info, error } = await fetchGetUserInfo(tokenInfo.token);

    if (!error) {
      // 2. store user info
      localStg.set('userInfo', info);

      // 3. update store
      token.value = tokenInfo.token;
      Object.assign(userInfo, info);

      return true;
    }

    return false;
  }

  async function loginWithEmail(mailAccount: string, verifyCode: string, redirect = true) {
    startLoading();

    const { data: tokenInfo, error } = await fetchLoginWithEmail(mailAccount, verifyCode);

    // console.log("tokenInfo==>", tokenInfo);

    if (!error) {
      const pass = await loginByToken(tokenInfo);

      if (pass) {
        await routeStore.initAuthRoute();

        if (redirect) {
          await redirectFromLogin();
        }

        if (routeStore.isInitAuthRoute) {
          window.$notification?.success({
            title: $t('page.login.common.loginSuccess'),
            content: $t('page.login.common.welcomeBack', { userName: userInfo.userName }),
            duration: 4500
          });
        }
      }
    } else {
      resetStore();
    }
    endLoading();
  }

  return {
    token,
    userInfo,
    isStaticSuper,
    isLogin,
    loginLoading,
    resetStore,
    login,
    loginWithCaptcha,
    loginWithEmail,
    confirmLogout
  };
});
