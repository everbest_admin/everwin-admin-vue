import { computed } from 'vue';
import { useCountDown, useLoading } from '@sa/hooks';
import { $t } from '@/locales';
import {REG_EMAIL, REG_PHONE} from '@/constants/reg';
import {fetchSendEmailCaptcha} from "@/service/api";

export function useCaptcha() {
  const { loading, startLoading, endLoading } = useLoading();
  const { count, start, stop, isCounting } = useCountDown(60);

  const label = computed(() => {
    let text = $t('page.login.codeLogin.getCode');

    const countingLabel = $t('page.login.codeLogin.reGetCode', { time: count.value });

    if (loading.value) {
      text = '';
    }

    if (isCounting.value) {
      text = countingLabel;
    }

    return text;
  });

  function isPhoneValid(phone: string) {
    if (phone.trim() === '') {
      window.$message?.error?.($t('form.phone.required'));
      return false;
    }

    if (!REG_PHONE.test(phone)) {
      window.$message?.error?.($t('form.phone.invalid'));
      return false;
    }
    return true;
  }

  async function getCaptcha(phone: string) {
    const valid = isPhoneValid(phone);
    if (!valid || loading.value) {
      return;
    }
    startLoading();

    // request
    await new Promise(resolve => {
      setTimeout(resolve, 500);
    });

    window.$message?.success?.($t('page.login.codeLogin.sendCodeSuccess'));
    start();
    endLoading();
  }

  function isEmailValid(mailAccount: string) {
    if (mailAccount.trim() === '') {
      window.$message?.error?.($t('form.email.required'));
      return false;
    }

    if (!REG_EMAIL.test(mailAccount)) {
      window.$message?.error?.($t('form.email.invalid'));
      return false;
    }
    return true;
  }

  async function getEmailCaptcha(mailAccount: string) {
    const valid = isEmailValid(mailAccount);
    if (!valid || loading.value) {
      return;
    }

    startLoading();

    // request
    await fetchSendEmailCaptcha(mailAccount);

    window.$message?.success?.($t('page.login.codeLogin.sendCodeSuccess'));
    start();
    endLoading();
  }

  return {
    label,
    start,
    stop,
    isCounting,
    loading,
    getCaptcha,
    getEmailCaptcha
  };
}
