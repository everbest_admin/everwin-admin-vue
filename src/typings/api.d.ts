/**
 * Namespace Api
 *
 * All backend api type
 */
declare namespace Api {
  namespace Common {
    /** common params of paginating */
    interface PaginatingCommonParams {
      /** current page number */
      current: number;
      /** page size */
      size: number;
      /** total count */
      total: number;
    }

    /** common params of paginating query list data */
    interface PaginatingQueryRecord<T = any> extends PaginatingCommonParams {
      records: T[];
    }

    /**
     * enable status
     *
     * - "1": enabled
     * - "0": disabled
     */
    type EnableStatus = '1' | '0';

    /** common record */
    type CommonRecord<T = any> = {
      /** record id */
      id: number;
      /** record creator */
      createBy: string;
      /** record create time */
      createTime: string;
      /** record updater */
      updateBy: string;
      /** record update time */
      updateTime: string;
      /** record status */
      status: EnableStatus | null;
    } & T;
  }

  /**
   * namespace Auth
   *
   * backend api module: "auth"
   */
  namespace Auth {
    interface CaptchaInfo {
      captcha: string;
      captchaKey: string;
    }

    interface LoginToken {
      token: string;
      refreshToken: string;
    }

    // sa-token return
    interface TokenInfo {
      tokenName: string;
      tokenValue: string;
      isLogin: boolean;
      loginId: string;
      loginType: string;
      tokenTimeout: number;
      sessionTimeout: number;
      tokenSessionTimeout: number;
      tokenActiveTimeout: number;
      loginDevice: string;
      tag: string;
    }

    interface UserInfo {
      userId: string;
      userName: string;
      roles: string[];
      buttons: string[];
    }
  }

  /**
   * namespace Route
   *
   * backend api module: "route"
   */
  namespace Route {
    type ElegantConstRoute = import('@elegant-router/types').ElegantConstRoute;

    interface MenuRoute extends ElegantConstRoute {
      id: string;
    }

    interface UserRoute {
      routes: MenuRoute[];
      home: import('@elegant-router/types').LastLevelRouteKey;
    }
  }

  /**
   * namespace SystemManage
   *
   * backend api module: "systemManage"
   */
  namespace SystemManage {
    type CommonSearchParams = Pick<Common.PaginatingCommonParams, 'current' | 'size'>;

    /** role */
    type Role = Common.CommonRecord<{
      /** role name */
      roleName: string;
      /** role code */
      roleCode: string;
      /** role description */
      roleDesc: string;
    }>;

    /** role search params */
    type RoleSearchParams = CommonType.RecordNullable<
      Pick<Api.SystemManage.Role, 'roleName' | 'roleCode' | 'status'> & CommonSearchParams
    >;

    /** role list */
    type RoleList = Common.PaginatingQueryRecord<Role>;

    /** all role */
    type AllRole = Pick<Role, 'id' | 'roleName' | 'roleCode'>;

    /**
     * user gender
     *
     * - "1": "male"
     * - "0": "female"
     */
    type gender = '1' | '0';

    /** user */
    type User = Common.CommonRecord<{
      /** user name */
      userName: string;
      loginName: string;
      /** user nick name */
      nickName: string;
      address: string;
      age: string;
      /** user gender */
      gender: gender | null;
      idNumber: string;
      birthday: string;
      /** user phone */
      telephone: string;
      /** user email */
      email: string;
      /** user role code collection */
      // eslint-disable-next-line no-warning-comments
      // TODO : 暂无权限控制
      userRoles: string[];
      status: string;
    }>;

    type Log = Common.CommonRecord<{
      id: string;
      userId: string;
      ip: string;
      methodName: string;
      requestMethod: string;
      requestUrl: string;
      description: string;
      params: string;
      requestTime: string;
    }>;

    type Poem = Common.CommonRecord<{
      id:string;
      poemTitle:string;
      author:string;
      dynasty:string;
      content:string;
      description:string;
      style:string;
      tag:string;
    }>;

    /** user search params */
    type UserSearchParams = CommonType.RecordNullable<
      Pick<Api.SystemManage.User, 'userName' | 'gender' | 'nickName' | 'telephone' | 'email' | 'status'> &
        CommonSearchParams
    >;

    /** log search params */
    type LogSearchParams = 'userId' | 'methodName' | 'requestMethod' | ('requestTimeRange' & CommonSearchParams);

    /** poem search params */
    type PoemSearchParams = 'poemTitle' | 'author' | 'dynasty' | 'content'| 'description'| 'style'| 'tag' & CommonSearchParams;


    /** user list */
    type UserList = Common.PaginatingQueryRecord<User>;

    /**
     * menu type
     *
     * - "1": directory
     * - "2": menu
     */
    type MenuType = '1' | '2';

    type MenuButton = {
      /**
       * button code
       *
       * it can be used to control the button permission
       */
      code: string;
      /** button description */
      desc: string;
    };

    /**
     * icon type
     *
     * - "1": iconify icon
     * - "2": local icon
     */
    type IconType = '1' | '2';

    type MenuPropsOfRoute = Pick<
      import('vue-router').RouteMeta,
      | 'i18nKey'
      | 'keepAlive'
      | 'constant'
      | 'order'
      | 'href'
      | 'hideInMenu'
      | 'activeMenu'
      | 'multiTab'
      | 'fixedIndexInTab'
      | 'query'
    >;

    type Menu = Common.CommonRecord<{
      /** parent menu id */
      parentId: number;
      /** menu type */
      menuType: MenuType;
      /** menu name */
      menuName: string;
      /** route name */
      routeName: string;
      /** route path */
      routePath: string;
      /** component */
      component?: string;
      /** iconify icon name or local icon name */
      icon: string;
      /** icon type */
      iconType: IconType;
      /** buttons */
      buttons?: MenuButton[] | null;
      /** children menu */
      children?: Menu[] | null;
    }> &
      MenuPropsOfRoute;

    /** menu list */
    type MenuList = Common.PaginatingQueryRecord<Menu>;

    type MenuTree = {
      id: number;
      label: string;
      pId: number;
      children?: MenuTree[];
    };
  }
}
